let password = document.getElementById('password');
let showPass = document.getElementById('show');
let hidePass = document.getElementById('hide');
let passwordConfirm = document.getElementById('passwordConfirm');
let btn = document.querySelector('.btn');
let error = document.getElementById("error");

showPass.addEventListener('click', function() {
    if(password.type === "password"){
        password.type = "text";
        password.closest("i").classList.add('fa-eye-slash');
        password.closest("i").classList.remove('fa-eye');
    }else {
        password.type === "password";
        password.closest("i").classList.remove('fa-eye-slash');
        password.closest("i").classList.add('fa-eye');
    }
});
hidePass.addEventListener('click', function() {
    if(passwordConfirm.type === "password"){
        passwordConfirm.type = "text";
        passwordConfirm.closest("i").classList.add('fa-eye-slash');
        passwordConfirm.closest("i").classList.remove('fa-eye');
    }else {
        passwordConfirm.type === "password";
        passwordConfirm.closest("i").classList.remove('fa-eye-slash');
        passwordConfirm.closest("i").classList.add('fa-eye');
    }
});

btn.addEventListener('click', function () {
    if(password.value !== passwordConfirm.value) {
        error.innerHTML = 'Нужно ввести одинаковые значения';
        error.classList.add('text-warning');
        error.classList.remove('hide');
    } else if(
        password.value === passwordConfirm.value 
        && password.value !== "" 
        && passwordConfirm.value !== "") {
        error.classList.add('hide');
        alert("You are welcome");
    }
});
